#!/usr/bin/env bash


virtualenv test1
source test1/bin/activate
pip install selenium
sudo apt-get install openjdk-9-jdk
wget https://chromedriver.storage.googleapis.com/2.35/chromedriver_linux64.zip
unzip -o chromedriver_linux64.zip

nosetests automatedtesting.py


